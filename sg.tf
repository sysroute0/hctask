resource "aws_security_group" "permit_web_elb" {
  name        = "permit_web"
  description = "Permit Web"
  vpc_id      = aws_vpc.hctask.id

  ingress {
    description      = "Allow HTTP from Internet"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "permit_web"
  }
}

resource "aws_security_group" "permit_mysql" {
  name        = "permit_mysql"
  description = "Permit MySQL"
  vpc_id      = aws_vpc.hctask.id

  ingress {
    description      = "Allow MySQL"
    from_port        = 3306
    to_port          = 3306
    protocol         = "tcp"
    cidr_blocks      = [var.vpc_cidr_block]
    ipv6_cidr_blocks = ["::/0"]
  }

  ingress {
    from_port   = 8
    to_port     = 0
    protocol    = "icmp"
    cidr_blocks = [var.vpc_cidr_block]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "permit_web"
  }
}

resource "aws_security_group" "ec2" {
  name        = "ec2_sg"
  description = "Allowed traffic to EC2 instance"
  vpc_id      = aws_vpc.hctask.id

  ingress {
    description      = "Allow SSH"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  ingress {
    description      = "Allow HTTP"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = [var.vpc_cidr_block]
    ipv6_cidr_blocks = ["::/0"]
  }

  ingress {
    from_port   = 8
    to_port     = 0
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "permit_web"
  }
}

resource "aws_security_group" "efs" {
  name        = "efs_sg"
  description = "Allow inbound EFS traffic from EC2"
  vpc_id      = aws_vpc.hctask.id

  ingress {
    security_groups = [aws_security_group.ec2.id]
    from_port       = 2049
    to_port         = 2049
    protocol        = "tcp"
  }

  egress {
    security_groups = [aws_security_group.ec2.id]
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
  }
}

