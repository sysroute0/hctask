resource "aws_db_subnet_group" "wordpress" {
  name       = "wordpress"
  subnet_ids = [aws_subnet.private-subnet-1a.id, aws_subnet.private-subnet-1b.id]
}


resource "aws_db_instance" "wordpress" {
  identifier             = "wordpress-db"
  allocated_storage      = 5
  engine                 = "mysql"
  engine_version         = "5.7.37"
  instance_class         = "db.t2.micro"
  db_name                = "wordpress"
  username               = "wpadmin"
  password               = "sup3rs3cr37p4s5"
  skip_final_snapshot    = true
  publicly_accessible    = false
  vpc_security_group_ids = [aws_security_group.permit_mysql.id]
  db_subnet_group_name   = aws_db_subnet_group.wordpress.id
}

