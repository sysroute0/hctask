#!/bin/bash
sudo sed -i 's/# deb /deb /g' /etc/apt/sources.list
sudo apt-get update
sudo apt-get upgrade -y
sudo apt-get dist-upgrade -y
sudo apt-get --purge autoremove -y
sudo apt-get install php php-gd php-mysql net-tools wget nfs-common -y
sudo echo "${nfs_address}:/ /var/www/html nfs4 defaults,_netdev 0 0" >> /etc/fstab
sudo mount -a
cd /tmp ; wget https://www.wordpress.org/latest.tar.gz
sudo tar -zxvf latest.tar.gz --strip 1 -C /var/www/html ; rm /tmp/latest.tar.gz
sudo touch /var/www/html/live.txt
sudo chown -R www-data. /var/www/html
sudo echo "web1" > /etc/hostname

sudo reboot
