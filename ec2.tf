resource "aws_instance" "web1" {
  ami           = var.ami_id
  instance_type = var.instance_type

  subnet_id              = aws_subnet.public-subnet-1a.id
  vpc_security_group_ids = [aws_security_group.ec2.id]
  availability_zone      = var.az1a

  associate_public_ip_address = true
  key_name                    = aws_key_pair.aws_ssh_key.key_name

  user_data = templatefile("web1.sh", { nfs_address = aws_efs_file_system.efs.dns_name })

}

resource "aws_instance" "web2" {
  ami           = var.ami_id
  instance_type = var.instance_type

  subnet_id              = aws_subnet.public-subnet-1b.id
  vpc_security_group_ids = [aws_security_group.ec2.id]
  availability_zone      = var.az1b

  associate_public_ip_address = true
  key_name                    = aws_key_pair.aws_ssh_key.key_name

  user_data = templatefile("web2.sh", { nfs_address = aws_efs_file_system.efs.dns_name })

}

resource "aws_key_pair" "aws_ssh_key" {
  key_name   = "ssh-key"
  public_key = file("id_rsa.pub")
}
