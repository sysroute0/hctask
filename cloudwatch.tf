resource "aws_cloudwatch_metric_alarm" "web_servers" {
  alarm_name          = "web_servers"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "2"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "60"
  statistic           = "Average"
  threshold           = "70"
  alarm_description   = "This metric monitors web servers CPU utilization exceeding 70%"
}

resource "aws_cloudwatch_composite_alarm" "EC2" {
  alarm_description = "Alarm that monitors CPUUtilization "
  alarm_name        = "EC2_Composite_Alarm"
  alarm_actions     = [aws_sns_topic.EC2_topic.arn]

  alarm_rule = "ALARM(${aws_cloudwatch_metric_alarm.web_servers.alarm_name})"

  depends_on = [
    aws_cloudwatch_metric_alarm.web_servers,
    aws_sns_topic.EC2_topic,
    aws_sns_topic_subscription.EC2_Subscription
  ]
}


resource "aws_sns_topic" "EC2_topic" {
  name = "EC2_topic"
}

resource "aws_sns_topic_subscription" "EC2_Subscription" {
  topic_arn = aws_sns_topic.EC2_topic.arn
  protocol  = "email"
  endpoint  = "martin_krastev@outlook.com"

  depends_on = [
    aws_sns_topic.EC2_topic
  ]
}

