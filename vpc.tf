provider "aws" {
  region = var.region
}

resource "aws_vpc" "hctask" {
  cidr_block           = var.vpc_cidr_block
  enable_dns_hostnames = true
  enable_dns_support   = true
  tags = {
    Name : "hctask"
  }
}

### Creating subnets
resource "aws_subnet" "public-subnet-1a" {
  vpc_id            = aws_vpc.hctask.id
  cidr_block        = var.public-subnet-1a
  availability_zone = var.az1a

  tags = {
    Name : "public-subnet-1a"
  }
}

resource "aws_subnet" "public-subnet-1b" {
  vpc_id            = aws_vpc.hctask.id
  cidr_block        = var.public-subnet-1b
  availability_zone = var.az1b

  tags = {
    Name : "public-subnet-1b"
  }
}

resource "aws_subnet" "private-subnet-1a" {
  vpc_id            = aws_vpc.hctask.id
  cidr_block        = var.private-subnet-1a
  availability_zone = var.az1a

  tags = {
    Name : "private-subnet-1a"
  }
}

resource "aws_subnet" "private-subnet-1b" {
  vpc_id            = aws_vpc.hctask.id
  cidr_block        = var.private-subnet-1b
  availability_zone = var.az1b

  tags = {
    Name : "private-subnet-1b"
  }
}

###

resource "aws_internet_gateway" "hctask-igw" {
  vpc_id = aws_vpc.hctask.id
}

### Creating route tables
resource "aws_route_table" "public-rtb" {
  vpc_id = aws_vpc.hctask.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.hctask-igw.id
  }

  tags = {
    Name : "public-rtb"
  }
}

resource "aws_route_table" "private-rtb" {
  vpc_id = aws_vpc.hctask.id

  tags = {
    Name : "private-rtb"
  }
}

###

### Adding subnets to the routing tables
resource "aws_route_table_association" "public-subnet-1a" {
  subnet_id      = aws_subnet.public-subnet-1a.id
  route_table_id = aws_route_table.public-rtb.id
}

resource "aws_route_table_association" "public-subnet-1b" {
  subnet_id      = aws_subnet.public-subnet-1b.id
  route_table_id = aws_route_table.public-rtb.id
}

resource "aws_route_table_association" "private-subnet-1a" {
  subnet_id      = aws_subnet.private-subnet-1a.id
  route_table_id = aws_route_table.private-rtb.id
}

resource "aws_route_table_association" "private-subnet-1b" {
  subnet_id      = aws_subnet.private-subnet-1b.id
  route_table_id = aws_route_table.private-rtb.id
}

###
