resource "aws_efs_file_system" "efs" {
  creation_token = "efs"
  encrypted      = "true"
  tags = {
    Name = "EFS Wordpress"
  }
}

resource "aws_efs_mount_target" "efs-wp-1a" {
  file_system_id  = aws_efs_file_system.efs.id
  subnet_id       = aws_subnet.private-subnet-1a.id
  security_groups = [aws_security_group.efs.id]
}

resource "aws_efs_mount_target" "efs-wp-1b" {
  file_system_id  = aws_efs_file_system.efs.id
  subnet_id       = aws_subnet.private-subnet-1b.id
  security_groups = [aws_security_group.efs.id]
}
