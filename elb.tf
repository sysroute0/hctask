resource "aws_elb" "elb-wp" {
  name            = "wp-elb"
  subnets         = [aws_subnet.public-subnet-1a.id, aws_subnet.public-subnet-1b.id]
  security_groups = [aws_security_group.permit_web_elb.id]

  listener {
    instance_port     = 80
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 5
    target              = "HTTP:80/live.txt"
    interval            = 30
  }

  instances                   = [aws_instance.web1.id, aws_instance.web2.id]
  cross_zone_load_balancing   = true
  idle_timeout                = 400
  connection_draining         = true
  connection_draining_timeout = 400

}

resource "aws_lb_cookie_stickiness_policy" "elb-wp-policy" {
  name                     = "elb-wp-policy"
  load_balancer            = aws_elb.elb-wp.id
  lb_port                  = 80
  cookie_expiration_period = 600
}

