variable "region" {
  description = "AWS Region"
  type        = string
  default     = "eu-central-1"
}

variable "az1a" {
  description = "Availability Zone 1A"
  type        = string
  default     = "eu-central-1a"
}

variable "az1b" {
  description = "Availability Zone 1B"
  type        = string
  default     = "eu-central-1b"
}

variable "vpc_cidr_block" {
  description = "VPC CIDR block"
  type        = string
  default     = "192.168.0.0/16"
}

variable "public-subnet-1a" {
  description = "Public subnet 1a"
  type        = string
  default     = "192.168.0.0/24"
}

variable "public-subnet-1b" {
  description = "Public subnet 1b"
  type        = string
  default     = "192.168.1.0/24"
}

variable "private-subnet-1a" {
  description = "Private subnet 1a"
  type        = string
  default     = "192.168.20.0/22"
}

variable "private-subnet-1b" {
  description = "Private subnet 1a"
  type        = string
  default     = "192.168.40.0/22"
}

variable "instance_type" {
  type    = string
  default = "t2.micro"
}

variable "ami_id" {
  description = "Ubuntu 22.04 in eu-central-1"
  type        = string
  default     = "ami-065deacbcaac64cf2"
}

