#!/bin/bash
sudo sed -i 's/# deb /deb /g' /etc/apt/sources.list
sudo apt-get update
sudo apt-get upgrade -y
sudo apt-get dist-upgrade -y
sudo apt-get --purge autoremove -y
sudo apt-get install php php-gd php-mysql net-tools wget nfs-common -y
echo "${nfs_address}:/ /var/www/html nfs4 defaults,_netdev 0 0" >> /etc/fstab
sudo mount -a
sudo echo "web2" > /etc/hostname

sudo reboot
