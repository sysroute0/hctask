output "vpc_id" {
  value = aws_vpc.hctask.id
}

output "elb_dns_hostname" {
  value = aws_elb.elb-wp.dns_name
}

output "efs_dns_hostname" {
  value = aws_efs_file_system.efs.dns_name
}

